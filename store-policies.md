# Privacy Policy

Jan Friedli built the Deine Rechte app as an Open Source app. This SERVICE is provided by Jan Friedli at no cost and is intended for use as is.

This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.

If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.

The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Deine Rechte unless otherwise defined in this Privacy Policy.


## Information Collection and Use

For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information. The information that I request will be retained on your device and is not collected by me in any way.

The app does NOT use third party services that may collect information used to identify you.


## Log Data

I want to inform you that whenever you use my Service, in a case of an error in the app I DO NOT collect data and information on your phone called Log Data.


## Cookies

Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.

This Service does not use these “cookies” explicitly.


## Service Providers
No third party service providers will be hired. This is free
software on which all people contribute for free.


## Security

I value your trust in providing us your Personal Information.
No data is sent over the network. The app works completely locally. 
It does not collect personal data in any form.


## Links to Other Sites

This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by me. Therefore, I strongly advise you to review the Privacy Policy of these websites. I have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.


## Children’s Privacy

These Services do not address anyone under the age of 13. I do not knowingly collect personally identifiable information from children under 13. In the case I discover that a child under 13 has provided me with personal information, I immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact me so that I will be able to do necessary actions.


## Changes to This Privacy Policy


I may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. I will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.


## Contact Us

If you have any questions or suggestions about my Privacy Policy, do not hesitate to open an issue on our issue [tracker](https://0xacab.org/jfriedli/your-rights-bern/-/boards)




This privacy policy page was created at privacypolicytemplate.net and modified by the repository owner.