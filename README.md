# Deine Rechte im Kanton Bern

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/packages/ch.gassenarbeit.bern.your.rights)

[![Semantic description of image](https://lh3.googleusercontent.com/cjsqrWQKJQp9RFO7-hJ9AfpKzbUb_Y84vXfjlP0iRHBvladwAfXih984olktDhPnFqyZ0nu9A5jvFwOEQPXzv7hr3ce3QVsLN8kQ2Ao=s0)](https://play.google.com/store/apps/details?id=ch.gassenarbeit.bern.your.rights)
[![Semantic description of image](https://developer.apple.com/app-store/marketing/guidelines/images/badge-example-preferred.png)](https://apps.apple.com/us/app/deine-rechte/id1507230818?ls=1)


> Auch Polizei, Staatsanwält_innen und private Sicherheitsdienste dürfen nicht alles.
> Kennst Du Deine Rechte, so kannst Du Dich besser wehren  und bist weniger
> leicht einzuschüchtern – beispielsweise wenn Du beschuldigt wirst, eine Straftat
> begangen zu haben, oder wenn Du weggewiesen
> wirst, weil Du angeblich Leute beim Shoppen störst.

Simple Quasar/Vue.js app which ships the brochure "Deine Rechte" of Gassenarbeit Bern
as mobile app.
It's available on F-Droid Store, App Store and Google Play Store.

Android builds are also availbe in this [repo](https://0xacab.org/jfriedli/your-rights-bern/-/tags)

# Releasing

### Android Build
https://quasar.dev/quasar-cli/developing-cordova-apps/publishing-to-store


1) Update version number in `package.json` and in `quasar.conf.js`.
2) Run the build process: `quasar build -m android`
3) Get the release key named: `your-rights.keystore`
4) Sign the unsigned apk:
```bash
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore your-rights.keystore ./dist/cordova/android/apk/release/app-release-unsigned.apk your_rights
```
5) `zipalign -v 4 ./dist/cordova/android/apk/release/app-release-unsigned.apk your_rights.apk`
6) Upload it to Google Play Store and attach it to the tag in this repo.

#### The fingerprint of the signing key:

* MD5: C9:C7:7C:32:64:66:E0:29:7E:44:66:72:B9:E9:4B:C6
* SHA1: 49:6F:FB:72:B7:EA:33:54:4D:19:8D:08:63:F1:D6:7A:C9:50:99:63
* SHA256: 80:49:73:68:6B:68:7C:34:4B:5E:2E:B3:A7:6F:76:5D:A1:DB:71:25:23:D0:9D:F5:E1:CB:CB:C6:87:B0:E0:49

#### F-Droid Release
F-Droid needs to have the build files in the repository.
Instead of running the build command above you'll have to build it like
this: `quasar build -m android -i`

Check in all files in src-cordova except for the build files...
See here to learn from past mistakes ;) https://gitlab.com/fdroid/fdroiddata/-/merge_requests/6601


Then check the generated folder into the repo: `git add src-cordova/platforms/android/ -f`
and push it.

Then you can follow: https://blog.fossasia.org/publish-an-open-source-app-on-fdroid/
Note that the import command has to look like this: `droid import --url https://0xacab.org/jfriedli/your-rights-bern.git --subdir src-cordova/platforms/android/app -v`

#### App Store Release

Get the provisioning file, and the certificate. Import the certificate into your osx keychanin.
Then crate a prod build by running: `quasar build -m ios`
If that was successful open the folder `src-cordova/platforms/ios` in XCode.
Select a generic iOS device for the build. In the build settings make sure to select the provisioning profile, the correct
apple id. Then click on Product - Archive to generate the build. Once this has finished you can click submit in
the new window that has popped up after the build. After that go to itunes store connect and update the build
details and submit it.


# Development
The software architecture is very simple.
The folder `markdown` contains every chapter of the brochure as single Markdown file.
By prefixing the filename you can order them and the filename is used for routing.
Additionally the first line of every markdown file is used as sidebar entry.

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```
