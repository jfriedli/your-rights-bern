export default {
  chapters: 'Kapitel',
  your_rights: 'Deine Rechte',
  no_search_result: 'Kein Suchtreffer',
  back: 'zurück',
  forward: 'weiter'
}
