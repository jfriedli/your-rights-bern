# BEWEISE

Du hast das Recht, bei Beweiserhebungen (z.B. Befragungen von Zeug\*innen) dabei zu sein und Fragen zu stellen oder durch deine Anwältin/deinen Anwalt stellen zu lassen.

Weitere Untersuchungshandlungen können zum Beispiel die Abnahme von Fingerabdrücken oder DNAProben (vgl. «Erkennungsdienstliche Massnahmen & DNA-Proben»), Hausdurchsuchungen (vgl. «Hausdurchsuchung») oder Überwachung Deines Post-, Telefon- oder Internetverkehrs sein (oft bei Verdacht auf Drogenhandel). Wenn die Staatsanwaltschaft nicht genügend Beweise hat, wird das Verfahren eingestellt und Du wirst nicht bestraft.
