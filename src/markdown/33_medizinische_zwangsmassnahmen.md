# MEDIZINISCHE ZWANGSMASSNAHMEN

Du wirst gezwungen, Medikamente zu nehmen, Du wirst isoliert oder angebunden oder man verbietet Dir den Kontakt zu bestimmten Personen: Das sind Beispiele für medizinische Zwangsmassnahmen. Sie sind nur erlaubt, wenn Du mit einer Fürsorgerischen Unterbringung (vgl. «Fürsorgerische Unterbringung») in eine Anstalt eingewiesen wurdest – aber nicht, wenn Du ambulant behandelt wirst. Es muss darum gehen, Deine Gesundheit oder andere Personen zu schützen.

Die Zwangsmassnahme muss von der Leitung der Institution angeordnet werden. Die Leitung darf sie nur anordnen, wenn freiwillige Massnahmen nicht funktioniert haben oder Du Dich freiwilligen Massnahmen verweigerst. Ausserdem muss sie das mildeste Mittel anwenden, das in Deinem Fall geeignet ist. Die behandelnden Ärzt\*innen müssen Dich umfassend über die Behandlung informieren.

Gegen die Massnahme kannst Du Dich innerhalb von 10 Tagen beim Erwachsenenschutzgericht der Zivilabteilung des Obergerichts wehren (siehe «Adressen»). Dabei kann Dich eine Dir nahestehende Person oder eine Anwältin/ein Anwalt vertreten.
