# UNTERSUCHUNGSHAFT

Die Staatsanwaltschaft kann Dich 48 Stunden in Haft nehmen, wenn neben einem dringenden Tatverdacht, dass Du ein Vergehen oder Verbrechen begangen hast, zusätzlich die Gefahr besteht, dass Du in Freiheit Beweise vernichten würdest oder Dich mit anderen Involvierten absprichst (Verdunkelungsgefahr), Du flüchtest (Fluchtgefahr), Du weitere Straftaten begehen (Wiederholungsgefahr) oder eine angekündigte Straftat ausführen wirst (Ausführungsgefahr).

Nach spätestens 48 Stunden muss die Staatsanwaltschaft beim Zwangsmassnahmengericht die Anordnung der Untersuchungshaft beantragen. Das Gericht hat danach eine Verhandlung durchzuführen. Wir empfehlen Dir, eine mündliche Verhandlung zu verlangen. Aussagen vor dem Zwangsmassnahmengericht können sich auf Dein Strafverfahren auswirken.

Wie immer im Strafverfahren hast Du das Recht auf eine Anwältin/einen Anwalt (vgl. «Recht auf Anwältin/ Anwalt»). Sie\*er darf Dich besuchen und Eure Briefe dürfen die Behörden nicht lesen. Die übrige Post wird aber kontrolliert.
