# SEXARBEIT

Sexarbeit ist in der Schweiz grundsätzlich legal, ist aber je nach Gemeinde örtlich eingeschränkt. Sexarbeit unter 18 Jahren ist verboten. Wer eine Person unter 18 Jahren der Sexarbeit zuführt, macht sich strafbar.

Seit dem 1. April 2013 gibt es im Kanton Bern ein Prostitutionsgesetz (PGG), eine Verordnung über das Prostitutionsgewerbe (PGV), bereits seit August 2003 eine Verordnung über die Strassenprostitution (SPV).

Bist Du Sexarbeiter\*in aus einem EU/EFTA-Staat, so brauchst Du im Kanton Bern eine Bewilligung der Migrationsbehörde. Um diese zu bekommen musst Du selbstständig erwerbend sein oder laut PGG einen Arbeitsvertrag haben.

Am besten lässt Du Dich von der XENIA beraten (siehe «Adressen»).
