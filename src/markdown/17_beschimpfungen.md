# BESCHIMPFUNGEN

Wirst Du von Polizist\*innen beschimpft und beleidigt, lasse Dich nicht provozieren – genau das wollen sie nämlich damit erreichen. Für eine allfällige spätere Anzeige ist es wichtig, dass Du Zeug\*innen hast. Nach körperlichen Übergriffen und Beschimpfungen solltest Du so schnell als möglich ein genaues Erinnerungsprotokoll schreiben. Das ist zum einen wichtig für die «Psychohygiene» und hilft Dir zum anderen, Dich an für eine allfällige Anzeige wichtige Details zu erinnern (vgl. «Hinschauen»).
