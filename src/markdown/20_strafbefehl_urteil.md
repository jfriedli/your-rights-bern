# STRAFBEFEHL / URTEIL

Die allermeisten Strafverfahren werden von der Staatsanwaltschaft mit einem Strafbefehl abgeschlossen. In diesem Fall erhältst Du einen Strafbefehl – meist eine (bedingte) Geldstrafe. Du kannst innerhalb von 10 Tagen schriftlich Einsprache erheben (am besten eingeschrieben). Erhältst Du nach der Einsprache eine Vorladung zur Staatsanwaltschaft oder vor das Gericht, musst Du erscheinen, sonst gilt die Einsprache automatisch als zurückgezogen und der Strafbefehl wird zum rechtskräftigen Urteil.

Grundsätzlich empfehlen wir, Einsprache gegen einen Strafbefehl zu erheben. So hast Du mehr Zeit, um über das weitere Vorgehen zu entscheiden. Du solltest die Akten bei der Staatsanwaltschaft einsehen. Wenn sich herausstellt, dass Du vor Gericht nur kleine Chancen hättest, kann die Einsprache wieder zurückgezogen werden. Auch in diesem Fall gilt dann der ursprüngliche Strafbefehl als Urteil.

Bei grösseren Straftaten entscheidet das Gericht, ohne dass zuerst ein Strafbefehl erlassen wird. Das Gericht entscheidet auch, wenn Du Einsprache gegen einen Strafbefehl erhoben hast. Es spricht Dich entweder frei oder schuldig und verhängt eine Strafe. Auch Gerichtsurteile kannst Du anfechten. Besprich dies mit deiner Anwältin/deinem Anwalt (siehe «Recht auf Anwältin/Anwalt»).
