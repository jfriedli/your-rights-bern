# HINSCHAUEN

Merken Polizist\*innen (oder private Sicherheitskräfte), dass sie beobachtet werden und dass die Beobachter\*innen die Rechtslage kennen, überlegen sie sich zweimal, was sie tun.

Beobachtest Du Übergriffe (Schlagen, Beschimpfungen, usw.) schau hin und nicht weg! Versuche selber ruhig zu bleiben. Drängelst Du Dich rein oder mischt Du Dich sonst wie aktiv ein, riskierst Du eine Busse wegen Hinderung einer Amtshandlung. Gewalt gegen Polizist\*innen bringt Dir ausser Problemen nichts. Oft ist es am besten, aus einer gewissen Distanz die Betroffenen über ihre Rechte zu informieren
(zum Beispiel über das Recht auf «Aussageverweigerung»).

Bist Du selber betroffen oder bist Du Beobachter\*in, dann merk Dir die Zeit, den Ort, was genau geschieht und die Namen der Polizist\*innen. Notiere Dir die Kontaktangaben von anderen Beobachter\*innen, falls es später Zeug\*innen braucht. Schreib all dies möglichst bald in einem Gedächtnisprotokoll auf. Insbesondere wenn du persönliche Details und Kontaktangaben von Personen darin festhältst, ist es wichtig, Dein Gedächtnisprotokoll sicher aufzubewahren. Egal ob Du Opfer oder Zeug\*in eines Übergriffs wurdest: Melde Dich bei einer Organisation, die Dir helfen kann (siehe «Adressen»). Bleiben
Übergriffe für die Täter\*innen ohne Folgen, werden sie ihr Verhalten kaum ändern.
