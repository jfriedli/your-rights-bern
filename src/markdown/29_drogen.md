# DROGEN

Heroin, Kokain, LSD, Ecstasy, Cannabis und weitere Substanzen sind illegal(isiert)e Drogen. Du kannst bis zu drei Jahre Gefängnis oder eine Geldstrafe bekommen, wenn Du diese Drogen unter anderem anbaust, herstellst, besitzt, aufbewahrst, transportierst, kaufst, verkaufst, handelst oder abgibst.

Wenn Du mengenmässig im grossen Stil (mind. 12 g reines Heroin, 18 g reines Kokain, 36 g reines Amphetamin, 200 LSD-Trips), als Teil einer Bande oder gewerbsmässig mit Drogen zu tun hast, wirst Du mit Gefängnis von mindestens einem Jahr bestraft. Diese Mindeststrafe riskierst Du auch, wenn Du in der Nähe von Schulen Drogen anbietest, abgibst oder verkaufst. Allerdings kann in all diesen Fällen die Strafe entsprechend gemildert werden, wenn Du süchtig bist und die Tat der Finanzierung des Eigenkonsums hätte dienen sollen.

Wenn Du unter 18-Jährigen Drogen anbietest, ab­gibst oder verkaufst, kannst Du mit Gefängnis bis zu drei Jahren oder einer Geldstrafe bestraft werden.

Der Eigenkonsum von Betäubungsmitteln wird in der Regel mit einer Busse bestraft. Bei erstmaligem Konsum und bei Bagatellfällen erhältst Du im Kanton Bern bei weichen Drogen (Cannabis, Haschisch, Marihuana, Ecstasy, Rohypnol) eine Busse ab Fr. 100, bei harten Drogen gibt es eine Busse ab Fr. 200. Die Bussen erhöhen sich bei Rückfällen und schwerem Verschulden.

Wenn Du eine nur geringfügige Menge für den Eigengebrauch vorbereitest oder an eine andere volljährige Person unentgeltlich abgibst, damit Ihr gemeinsam konsumieren könnt, wirst Du nicht bestraft. Als geringfügig gelten bei Cannabis 10 Gramm.

Für Jugendliche unter 18 Jahren gelten andere Strafmasse und auch bei Bagatellfällen wird ein Jugendstrafverfahren gegen Dich eröffnet.

Wirst Du im Zusammenhang mit Widerhandlungen gegen das Betäubungsmittelgesetz kontrolliert und hast Du Geld dabei, kann es sein, dass ein Teil dieses Geldes zur Kostendeckung sichergestellt wird. Für ein solches Bussendepot darf Dir nicht Dein Sozialhilfegeld weggenommen werden (vgl. «Bussendepot»).
