# STRASSENMUSIK / STRASSENTHEATER

Die Bewilligungspflicht von Musik und Theater im öffentlichen Raum wird von den Gemeinden geregelt und kann sich von Ort zu Ort unterscheiden.

Willst Du in der Stadt Bern auftreten, dann brauchst Du eine Bewilligung, wenn Du mit mehr als einer wei- teren Person oder mehr als einmal in der Woche auftrittst und falls Du aktiv Geld sammelst (einen Hut hinstellen ist nicht aktives Sammeln).

Verstärker sind generell verboten. Nach 30 Minuten Auftritt musst Du den Standort wechseln. Bei einem Verstoss (auch gegen die Bewilligungspflicht) kannst Du mit höchstens 2000 Fr. gebüsst werden.

Eine Bewilligung kannst Du beim Polizeiinspektorat einholen. Wenn sie Dein Gesuch ablehnen, kannst Du das innert 30 Tagen bei der Direktion SUE anfechten (siehe «Adressen»).
