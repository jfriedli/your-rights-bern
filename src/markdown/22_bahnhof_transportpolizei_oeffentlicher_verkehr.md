# BAHNHOF / TRANSPORTPOLIZEI / ÖFFENTLICHER VERKEHR

In Bahnhöfen gilt die Hausordnung der SBB. Hältst Du Dich nicht daran, riskierst du ein Hausverbot. Unter anderem sind das Sitzen und Liegen auf Treppen und am Boden, Betteln, Hunde ohne Leine und «ungebührliches Verhalten» verboten. Wenn Du ein Hausverbot hast, darfst Du immer noch durch den Bahnhof zu einem Zug gehen oder Billette kaufen. Hältst Du Dich aber aus einem anderen Grund im Bahnhof auf, riskierst Du aber eine Anzeige wegen Hausfriedensbruch.

In Bern gehört ein Teil des Bahnhofs nicht der SBB, sondern ist öffentlicher Grund (Neuengass- und Teile der Christoffelunterführung). Dort gilt das Bahnhofreglement, das sich inhaltlich kaum von der SBB-Hausordnung unterscheidet. Weil es sich aber um öffentlichen Grund handelt, kannst Du hier kein Hausverbot bekommen, Du kannst aber weggewiesen werden (vgl. «Wegweisungen/Perimeterverbot»). Es patrouilliert – wie im SBB-Teil auch – die Securitrans (vgl. «Ordnungs- und Sicherheitsdienste»).

In Zügen ist die uniformierte Transportpolizei anzutreffen. Sie wird unter anderem gegen Schwarz­ fahrer\*innen eingesetzt (in Zügen kann man kein Billett mehr lösen). Die Transportpolizei hat ähnliche Befugnisse wie die Polizei, darf Dich also auch festnehmen.
