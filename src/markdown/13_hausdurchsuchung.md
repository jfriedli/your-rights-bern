# HAUSDURCHSUCHUNG

Grundsätzlich braucht es für eine Hausdurchsuchung einen schriftlichen Befehl der Staatsanwaltschaft oder des Gerichts, ausnahmsweise kann die Polizei auch ohne schriftlichen Befehl durchsuchen (bei «Gefahr in Verzug»).

Eine Hausdurchsuchung darf nur in Räumen durchgeführt werden, die Du benützt, nicht aber in den Zimmern Deiner Mitbewohner\*innen (ausser der Durchsuchungsbefehl gilt explizit auch für sie). Der Hausdurchsuchungsbefehl muss genau bezeichnen, welche Räume durchsucht werden dürfen. Du hast grundsätzlich das Recht, bei der Hausdurchsuchung dabei zu sein.

Von der Durchsuchung muss ein Protokoll angefertigt werden, insbesondere müssen die sichergestellten Gegenstände aufgelistet werden. Bei persönlichen Aufzeichnungen (Agenda, Tagebuch) oder Speichermedien (Computer, Handy) solltest du verlangen, dass diese versiegelt werden (vgl. «Beschlagnahmung»).


