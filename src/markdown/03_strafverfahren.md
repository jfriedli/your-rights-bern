# STRAFVERFAHREN

Wenn Dich die Polizei oder die Staatsanwaltschaft einer konkreten Straftat verdächtigt oder wenn Dich jemand anzeigt, wird ein Strafverfahren eingeleitet. Für das ganze Verfahren gilt die Schweizerische Strafprozessordnung (StPO). Du giltst als unschuldig, solange nicht das Gegenteil bewiesen ist.

Während der Untersuchung sammeln die Staatsanwaltschaft und die Polizei Beweise, wobei Befragungen zu den wichtigsten Informationsquellen gehören. Als beschuldigte Person hast Du das Recht auf Aussageverweigerung, d.h. Du musst keine Fragen beantworten (vgl. «Aussageverweigerung»).

Als so genannte Auskunftsperson hast Du dieses Recht auch, als Zeug\*in hingegen nur unter gewissen Umständen (vgl. «Zeug\*innen/Auskunftspersonen»). Wir empfehlen, insbesondere gegenüber der Polizei vom Recht auf Aussageverweigerung Gebrauch zu machen. Wenn nötig kannst Du Dich in einem späteren Zeitpunkt des Verfahrens immer noch äussern und Deine Sicht darlegen.
