# KINDER UND JUGENDLICHE

Als Kind oder Jugendliche\*r hast Du im öffentlichen Raum grundsätzlich die gleichen Rechte und Pflichten wie Erwachsene. Im Kanton Bern kann Dich die Polizei zu Deinen Eltern bringen, wenn Du noch nicht 18-jährig bist und es Dein Schutz oder der Schutz anderer Personen erfordert. In einigen Gemeinden gibt es Ausgangssperren für Kinder und Jugendliche. Das bedeutet, dass Jugendliche bis zu einem gewissen Alter ab einer bestimmten Uhrzeit (meist 22 Uhr) nur noch in Begleitung ihrer Eltern (oder anderen Betreuungspersonen) auf der Strasse sein dürfen.

Wenn Du noch nicht 18-jährig bist und eine Straftat begangen hast (z.B. Diebstahl oder ein Drogendelikt), kommt das Jugendstrafrecht zur Anwendung. In diesem Fall werden Deine Eltern über das Strafverfahren informiert – auch wenn Du das nicht willst. Du kannst im Verfahren jederzeit verlangen, dass eine Vertrauensperson dabei ist (dies müssen nicht Deine Eltern sein, wenn Du das nicht willst) und dass eine Anwältin/ein Anwalt Dich verteidigt. Auch Du darfst gegenüber der Polizei die Aussage verweigern, was wir Dir in einem ersten Schritt empfehlen (vgl. «Aussageverweigerung»). Deine gesetzlichen Vertreter\*innen (d.h. meistens Deine Eltern) können sich am Verfahren beteiligen. Wenn Deine Eltern und Du nicht dasselbe wollen, hast Du das Recht auf eine eigene Anwältin/einen eigenen Anwalt. Du kannst Dich für Informationen oder für die Vermittlung einer eigenen Strafverteidigung jederzeit an die Kinderanwaltschaft Schweiz (vgl. «Adressen») wenden.

Ausser Deine Eltern dürfen die Behörden ohne Dein Einverständnis keine anderen Personen über ein Strafverfahren gegen dich informieren (auch nicht Deine\*n Arbeitgeber\*in oder die Schule).

Nicht um Dich zu bestrafen, sondern um Dich «zu schützen», können die Behörden während des Verfahrens sogenannte Schutzmassnahmen anordnen. Sie lassen dich von einer bestimmten Person beaufsichtigen oder bringen Dich bei einer Privatperson oder in einem Heim unter. In ein geschlossenes Heim können sie Dich nur platzieren, wenn es für Deinen Schutz oder den Schutz anderer absolut notwendig ist.

Wirst Du verurteilt, so sind verschiedene Strafen oder Massnahmen möglich: Ein Verweis, Teilnahme an Kursen (z.B. Gewaltpräventionskurse), Erbringung einer persönlichen Leistung (z.B. zu Gunsten einer sozialen Einrichtung oder der geschädigten Person). Zur persönlichen Leistung darf man Dich nur verurteilen, wenn Du damit einverstanden bist (Dauer bei unter 15-jährigen: höchstens 10 Tage, bei über 15-jährigen höchstens drei Monate).

Bist Du 15-jährig, kannst Du zusätzlich mit einer Busse oder sogar mit Freiheitsentzug bestraft werden, du kannst für höchstens ein Jahr eingesperrt werden. Bist Du 16- bis 18-jährig und hast ein schweres Delikt wie z.B. schwere Körperverletzung, Raub oder Vergewaltigung begangen, kannst Du bis zu vier Jahren eingesperrt werden.

Wird eine Massnahme angeordnet (z.B. Fremdplatzierung), hast Du Anspruch darauf, dass jährlich geprüft wird, ob die Massnahme noch notwendig ist oder ob sie aufgehoben werden kann. Jede Massnahme endet spätestens an Deinem 22. Geburtstag.

Jugendliche können je nach Delikt bei einer Verurteilung ins Strafregister eingetragen werden (für Behörden einsehbar). Solche Straftaten erscheinen nicht im privaten Strafregisterauszug (also derjenige Auszug, den Du allenfalls bei einer\*einem Arbeitgeber\*in o.ä. einreichen musst). Ein Eintrag erscheint nur, wenn Du als erwachsene Person erneut straffällig geworden bist und Du deshalb wieder einen Eintrag ins Strafregister erhältst.

Wenn Du vor dem 10. Geburtstag eine Straftat begehst, kommt es zu keinem Strafverfahren, es werden lediglich Deine Eltern informiert.

Sexuelle Handlungen mit Personen unter 16 Jahren sind in der Schweiz verboten, ausser der Altersunterschied zwischen den Sexualpartner\*innen beträgt weniger als drei Jahre.
