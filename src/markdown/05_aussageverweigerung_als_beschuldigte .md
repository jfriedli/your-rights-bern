# AUSSAGEVERWEIGERUNG (ALS BESCHULDIGTE PERSON)

Wir empfehlen Dir, gegenüber der Polizei und der Staatsanwaltschaft Deine Aussage zu verweigern – egal, ob Du bei einer Straftat erwischt wurdest oder man Dir einfach etwas anhängen will. Mit der Aussageverweigerung schützt Du Dich selbst und andere Personen. Es ist besser nichts zu sagen, als Dich in Lügen zu verstricken.

Du kannst später vor Gericht aussagen, nachdem Du die Akten einsehen konntest und mit anderen Personen und/oder einer Anwältin/einem Anwalt gesprochen hast. Vor Gericht kennst Du alle Beweise, die tatsächlich gegen Dich vorliegen.

Aussageverweigerung ist Dein Recht. Polizist\*innen, die Dir anderes erzählen, bluffen. Vielmehr musst Du vor einer Befragung auf dieses Recht hingewiesen werden. Wird das nicht gemacht, sind Deine Aussagen nicht verwertbar.

Du hast das Recht jede Mitwirkung zu verweigern. D.h. beispielsweise, dass Du Deinen Handy-Entsperrcode oder Dein Computerpasswort nicht sagen musst. Wir empfehlen, diese Codes nicht ohne Absprache mit einer Anwältin/einem Anwalt offenzulegen.

Angeben musst Du nur Deine Personalien (Name, Vorname, Geburtsdatum, Adresse, Heimatort). Auf alle anderen Fragen antwortest Du am besten mit folgendem Satz: «Ich mache keine Aussage». Sätze wie
«ich weiss nicht mehr» oder auch nur «ja» und «nein» sind inhaltliche Aussagen und können später gegen Dich verwendet werden.

Wenn Du trotzdem eine Aussage machst, solltest Du nachfragen, wenn Du eine Frage nicht richtig verstanden hast. Überlege Dir genau, was Du sagen willst, bevor Du sprichst. Deine Aussagen werden protokolliert (vgl. «Befragungsprotokolle»).
