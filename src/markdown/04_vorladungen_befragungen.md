# VORLADUNGEN/BEFRAGUNGEN

Um Dich vorzuladen, muss sich die Polizei nicht an eine spezielle Formvorschrift halten. Das heisst sie kann Dir einen einfachen Brief schicken oder Dich telefonisch kontaktieren. Gehst Du nicht hin, kannst Du polizeilich vorgeführt werden – aber nur, wenn Dir das vorher schriftlich angedroht wurde.

Die Polizei oder die Staatsanwaltschaft muss Dich zu Beginn der ersten Befragung über Deine Rechte aufklären. Tut sie das nicht, darf die Befragung nicht verwertet werden, sie ist also ungültig. In Bern erhältst Du normalerweise ein Merkblatt, in dem Dir Deine Rechte erklärt werden. Lies es sorgfältig durch;  wenn Du etwas nicht verstehst, frag nach.

Wenn Du das Merkblatt erst nach statt vor der ersten Befragung erhältst oder Dir zusätzlich etwas erklärt wird, notiere das für Dich auf dem Merkblatt und verlange, dass es ins Protokoll geschrieben wird – es kann später für Dich als Beweismittel dienen (vgl. «Befragungsprotokolle»).

Du hast das Recht auf Übersetzung in eine Sprache, die Du verstehst.
