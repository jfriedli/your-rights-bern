# AUSSAGEN VON AUSKUNFTSPERSONEN / ZEUG*INNEN

Auch als Auskunftsperson hast Du das Recht, die Aussage zu verweigern. Als Auskunftsperson wirst Du vorgeladen, wenn noch nicht klar ist, ob Du zu den im Verfahren Beschuldigten gehören oder ob Du nur Zeug\*in sein wirst. Auch Mitangeschuldigte werden in den Verfahren der jeweils anderen als Auskunftspersonen befragt. Ebenso, wenn Du unter 15-jährig, selber das Opfer bist oder wenn Deine Urteilsfähigkeit eingeschränkt ist. Wir empfehlen, von Deinem Recht auf Aussageverweigerung Gebrauch zu machen (vgl. «Aussageverweigerung»).

Wirst Du als Zeug\*in vorgeladen, musst Du aussagen und Dich an die Wahrheit halten. Deine Aussage darfst Du nur verweigern, wenn Du mit der beschuldigten Person im Konkubinat zusammenlebst, mit ihr verheiratet bist, mit ihr Kinder hast, ihr nahe verwandt seid oder Du an ein Berufsgeheimnis gebunden bist.

Du musst Dich nicht selber belasten. Das heisst, dass Du die Aussage verweigern darfst, wenn Dir wegen der Aussage ein straf- oder zivilrechtliches Verfahren drohen würde. Es kann Dir nicht zum Vorwurf gemacht werden, wenn Du Dich an etwas nicht mehr erinnern kannst.
