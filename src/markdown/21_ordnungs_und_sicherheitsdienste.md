# ORDNUNGS- UND SICHERHEITSDIENSTE

Sie tragen Uniformen und arbeiten für Sicherheitsdienste wie «Securitas», «Protectas» oder «BroncosSecurity». Die Mitarbeiter\*innen dieser Firmen haben aber nicht mehr Rechte als Du. Sie dürfen Deinen Ausweis nicht kontrollieren, Deine Taschen nicht durchsuchen, Dich nicht festnehmen und Dich auch nicht aus dem öffentlichen Raum wegweisen.

Sie dürfen Dich aber festhalten, wenn sie Dich bei einer Straftat erwischen. Sie müssen dann sofort die Polizei herbeirufen.

Sicherheitsdienste in Bars, Restaurants und Läden setzen das Hausrecht der Betreiber\*innen durch. Deswegen dürfen sie Dich rauswerfen, wenn Dein Verhalten es rechtfertigt (vgl. «Hausverbot»). Ladendetektiv\*innen wollen bei Verdacht in Deine Taschen schauen. Du kannst Dich weigern und die Polizei verlangen.

In Bahnhöfen triffst Du neben der Transportpolizei (ehemals Bahnpolizei) auch auf die «Securitrans». Anders als andere private Sicherheitsleute dürfen sie Deinen Ausweis kontrollieren, Dich anhalten und Dich wegweisen, falls Du gegen die Hausordnung verstossen hast (vgl. «Bahnhof/Transportpolizei/öffentlicher Verkehr»).

In der Stadt Bern patrouilliert der städtische Ordnungsdienst «Pinto». Auch Pinto-Mitarbeiter\*innen haben nicht mehr Rechte als Du. Sie arbeiten eng mit der Polizei zusammen.
