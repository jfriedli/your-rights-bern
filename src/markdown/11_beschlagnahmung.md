# BESCHLAGNAHMUNG

Die Polizei darf Dir Sachen wegnehmen, wenn diese zur Begehung einer Straftat dienen könnten (z.B. Drogen­ utensilien, Vermummungsmaterial oder Einbruchswerkzeug) oder es sich z.B. um Diebesgut handelt. Die Polizei muss Dir mitteilen, wieso sie etwas sicherstellt und eine Liste erstellen. Manchmal wird auch Geld sichergestellt – mit der Begründung es sei illegal (z.B. durch Drogenhandel) beschafft worden (vgl. «Bussendepot»).

Verlange eine Kopie der Liste der sichergestellten Gegenstände, damit Du etwas in der Hand hast, wenn Du später die Herausgabe verlangst (etwa wenn Du beweisen kannst, dass das Geld nicht illegal beschafft wurde). Danach kann die Staatsanwaltschaft die Beschlagnahmung anordnen, wenn sie die Gegenstände länger behalten will, um sie z.B. als Beweismittel in einem Verfahren zu verwenden.

Will die Polizei oder die Staatsanwaltschaft Gegenstände sicherstellen bzw. beschlagnahmen (z.B. Computer, Handys, Notizbücher), empfehlen wir Dir sofort (schon gegenüber der Polizei) zu verlangen, dass diese Sachen unter Verschluss kommen (Siegelung). Versiegelte Gegenstände dürfen von den Behörden erst mit Erlaubnis des Zwangsmassnahmengerichts angeschaut bzw. ausgewertet werden. Achte darauf, dass Dein Antrag auf Siegelung schriftlich von der Polizei festgehalten wird (vgl. «Befragungsprotokoll»).

Bei Handy und Computer gilt, dass Du keine Zugangscodes oder Passwörter nennen musst, wenn sie beschlagnahmt werden. Wir empfehlen, Codes nicht ohne Absprache mit einer Anwältin/einem Anwalt offenzulegen (vgl. «Aussageverweigerung»).
