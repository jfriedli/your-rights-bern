# FESSELN

Die Polizei darf Dich u.a. fesseln, wenn Du Widerstand leistet, wenn der Verdacht besteht, Du würdest Menschen angreifen oder Sachen/Tiere beschädigen, wenn Du Beamt\*innen drohst, wenn Du den Verdacht erweckst, dass Du flüchten wirst oder wenn Du allgemein als gefährlich eingeschätzt wirst. Zudem ist die Fesselung auch bei Transporten zulässig.

Sind die Handschellen oder Kabelbinder zu fest angezogen, dann verlange, dass sie gelockert werden. Führt die Fesselung zu Verletzungen (z.B. Blutergüssen an den Handgelenken), solltest Du Dich sofort nach der Freilassung untersuchen und Dir ein ärztliches Attest machen lassen (vgl. «Verletzungen»).
