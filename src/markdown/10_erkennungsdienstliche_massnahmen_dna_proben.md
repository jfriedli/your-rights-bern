# ERKENNUNGSDIENSTLICHE MASSNAHMEN (ED) UND DNA-PROBEN

Zur Feststellung der Identität (vgl. «Personenkontrolle/vorläufige Festnahme») aber auch zur Aufklärung von Straftaten kann Dich die Polizei erkennungsdienstlich behandeln. Das heisst, sie darf Deine Fingerabdrücke nehmen, Dich fotografieren oder körperliche Merkmale ausmessen. Verweigerst Du die Behandlung, dann entscheidet die Staatsanwaltschaft in der Regel schriftlich. Ordnet die Staatsanwaltschaft die ED an, kann die Polizei diese auch zwangsweise durchführen. Wir empfehlen, auf eine schriftliche Anordnung der Staatsanwaltschaft zu bestehen.

Schrift- und Sprachproben dürfen nur mit Deiner Zustimmung genommen werden. Wir empfehlen, die Zustimmung zu verweigern.

Zur Aufklärung eines Verbrechens oder eines Vergehens kann die Polizei eine DNA-Probe entnehmen, wenn ein konkreter Verdacht gegen Dich besteht. Sie tut das normalerweise mit einem Wangenschleimhautabstrich (WSA). Die Kompetenz die Probenahme anzuordnen liegt alleine bei der Polizei – das heisst, wenn Du Dich weigerst, kann die Polizei die Probenahme auch ohne Anordnung der Staatsanwaltschaft zwangsweise durchsetzen. Wir empfehlen, trotzdem eine schriftliche Anordnung der Staatsanwaltschaft zu verlangen. Die Erstellung eines DNA-Profils und damit die Auswertung der Probe darf hingegen immer nur die Staatsanwaltschaft anordnen.

Bist Du der Ansicht, die DNA-Probe respektive Profilerstellung oder die ED seien unrechtmässig geschehen, kannst Du das polizeiliche Vorgehen oder die schriftliche Anordnung der Staatsanwaltschaft innert 10 Tagen mittels Beschwerde beim kantonalen Obergericht anfechten und die Löschung der Daten beantragen.

Löschung: Wurde eine ED nur zur Feststellung der Identität durchgeführt, müssen die Daten wieder gelöscht werden, sobald Deine Identität geklärt ist. Befindest Du Dich in einem Strafverfahren und dieses wird eingestellt oder Du wirst freigesprochen empfehlen wir Dir, die Löschung der Daten zu verlangen. Dieser Antrag ist schriftlich an das Bundesamt für Polizei zu stellen und am besten in Kopie an die\*den eidgenössischen Datenschutzbeauftragte*n (EDÖB) zu schicken.

Wirst Du verurteilt, bleiben die Daten für eine bestimmte Frist bei den Behörden gespeichert.
