# BEFRAGUNGSPROTOKOLLE

Wenn Du Dich entscheidest – aus welchem Grund auch immer – Aussagen zu machen, dann solltest Du darauf achten, dass das von Dir Gesagte richtig aufgeschrieben wird.

Befragungen kommen in der Form schriftlicher Befragungsprotokolle in die Strafakten und sind wichtige Beweismittel. Deshalb ist es wichtig, dass das Protokollierte tatsächlich dem Gesagten entspricht. Von Befragungen werden keine wortwörtlichen Mitschriften angefertigt, sondern sie geben bloss sinngemäss wieder, was Du gesagt hast.

Du hast das Recht, nach der Befragung das Protokoll entweder selber zu lesen oder es wird Dir vorgelesen. Wurde die Befragung mit Übersetzung durchgeführt, wird das Protokoll rückübersetzt.

So oder so ist es enorm wichtig, dass Du gut aufpasst und Korrekturen oder Anmerkungen anbringst, wenn Du nicht einverstanden bist. Werden Befragungen in Schweizerdeutsch durchgeführt, kann es sinnvoll sein, im Protokoll einzelne Worte in Schweizerdeutsch aufzuschreiben, wenn es kein passendes hoch­deutsches Wort gibt.

Im Protokoll können auch andere Anmerkungen fest­gehalten werden. Wenn du einen Antrag stellst, etwas kritisierst oder ähnliches, achte darauf, dass es im Protokoll festgehalten wird. Wenn Dir z.B. das Merkblatt mit Deinen Rechten als beschuldig­te Personen erst nach statt vor der Befragung ausge­händigt wird, wenn Dir keine Pause gewährt wird oder wenn Du die Siegelung Deines Tage­buch verlangst (vgl.
«Beschlagnahmung»), dann verlange eine entsprechende Anmerkung im Protokoll.

Wenn Du Deine Aussage unter Schock oder auf Entzug gemacht hast, dann verlan­ge, dass das ins Protokoll geschrieben wird. Du bist nicht verpflichtet, das Protokoll zu unterschreiben.
