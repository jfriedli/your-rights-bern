# WEGWEISUNG / PERIMETERVERBOT

Mit den neuen Polizeigesetz kann die Kantonspolizei eine oder mehrere Personen von einem Ort vorüber­ gehend wegweisen oder fernhalten, u.a. wenn die öffentliche Sicherheit und Ordnung, insbesondere durch eine Ansammlung, gestört oder gefährdet wird.

Der neue Wortlaut gibt der Polizei die Möglichkeit, auch Einzelpersonen, die nicht Teil einer Ansammlung sind, wegzuweisen.

Wie die Rechtsprechung diese neue Bestimmung anwenden wird, muss sich noch zeigen. Bisher war es so: Wenn Passant\*innen sich gestört fühlten, weil Personen an einem Ort sitzen, miteinander reden und Alkohol trinken, war das nicht genug für eine Wegweisung. Anders ausgedrückt: (Theoretisch) dürftet ihr nicht weggewiesen werden, nur weil ihr nicht ins Bild passt. Wenn aber viel Abfall rundliegt, wenn Leute herumschreien, Passant\*innen angepöbelt werden oder aus der Gruppe heraus «aggressiv» gebettelt wird, dann kann die Polizei Personen wegweisen. Wenn Du alleine, als Einzelperson, weggewiesen wurdest, dann melde Dich bei der Gassenarbeit, damit ihr besprechen könnt, ob eine Beschwerde sinnvoll ist.

Das neue Polizeigesetz sieht vor, dass Wegweisungen und Fernhaltungen bis zu einer Dauer von 48 Stunden mündlich angeordnet werden können. Umgekehrt heisst das, dass längere Wegweisungen grundsätzlich schriftlich zu erfolgen haben. Auch im Falle einer kurzen Wegweisung, kann nachträglich eine schriftliche Begründung verlangt werden. Wenn Du Fragen zum Vorgehen hast, kannst Du Dich an die Gassenarbeit wenden (vgl. «Adressen»).

Wirst Du weggewiesen, solltest Du darauf bestehen, dass dies schriftlich geschieht. In der Wegweisungsverfügung muss die Polizei beschreiben, wie die öffentliche Sicherheit und Ordnung konkret gestört wurde. Es genügt nicht zu schreiben, dass die Gruppe gestört hat. Sie muss auch schreiben, wie lange du dich in einem bestimmten Gebiet nicht mehr aufhalten darfst. Nach altem Recht waren das typischerweise drei Monate. Ob das neue Polizeigesetz eine Praxisänderung bringen wird, wird sich zeigen. In der Stadt Bern wird das Gebiet, in dem du dich nicht aufhalten darfst, «Perimeter» genannt, daher auch die Bezeichnung «Perimeterverbot».

Du kannst gegen die Wegweisungsverfügung Beschwerde einreichen (z.B. mit der Beschwerdevorlage der «Demokratischen Jurist\*innen Bern», siehe Adressen). Das kannst Du innerhalb von 30 Tagen bei der kantonalen Sicherheitsdirektion tun (siehe «Adressen»). Wenn über Deine Beschwerde entschieden wird, ist die Wegweisungsdauer häufig schon vorbei. Gibt Dir die Behörde trotzdem Recht, kann eventuell ein Grundsatzentscheid erwirkt werden, der die unklare Ausgestaltung und Anwendung des Wegweisungsartikels präzisiert.

Wenn du dich im verbotenen Perimeter aufhälst und dabei erwischt wirst, riskierst Du einen Strafbefehl wegen Ungehorsams gegen eine amtliche Verfügung
(vgl. «Strafbefehl/Urteil»).

Eine Beschwerde gegen die Wegweisungsverfügung und eine Einsprache gegen einen Strafbefehl sind u.U. mit Kosten verbunden. Wir empfehlen grundsätzlich, Dich anwaltlich oder von der Gassenarbeit beraten zu lassen (siehe «Adressen»). Handle rasch, wenn Du die Fristen nicht verpassen willst.

Auch rund um Demonstrationen oder Sportveranstaltungen greift die Polizei immer wieder auf Wegweisungen zurück und verbietet, «auffälligen Personen» sich für eine gewisse Zeit z.B, in der Innenstadt oder in der Nähe eines Stadions aufzuhalten.
