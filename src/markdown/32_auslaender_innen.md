# AUSLÄNDER*INNEN

Wenn Du als Tourist\*in in die Schweiz einreisen willst, brauchst Du einen Personalausweis/Pass und je nachdem woher Du kommst ein Visum. Du musst möglicherweise nachweisen, dass Du das für Deinen Auf­enthalt in der Schweiz notwendige Geld zur Ver­ fügung hast. Ohne private Unterkunft geht die Frem­ den­polizei von 100 Fr. pro Aufenthaltstag aus.

Als Ausländer\*in ist die Gefahr erfahrungsgemäss grösser, Probleme mit der Polizei zu kriegen – besonders, wenn Du nicht wie eine «typische Schweizer\*in» aussiehst. Eine Verurteilung kann für Ausländer\*innen neben den strafrechtlichen Auswirkungen auch Konsequenzen für den Ausweis/Aufenthalt haben. Die Rechtslage ist komplex und unterliegt stetigem Wandel, deshalb wird an dieser Stelle auf weitere Ausführungen verzichtet. Vergiss aber nicht, dass Du Rechte hast. Insbesondere hast Du immer die Möglichkeit, Beschwerde gegen ausländerrechtliche Massnahmen einzulegen. Lass Dich von einer Fachanwältin/einem Fachanwalt beraten.

Sollte Dir im Zusammenhang mit einem Strafverfahren eine Landesverweisung drohen, dann ist das ein Fall einer «notwendigen Verteidigung». Das heisst, Du bekommst auf jeden Fall eine Strafverteidigung. In einem solchen Fall ist zu empfehlen, dass die Strafverteidigung migrationsrechtliche Erfahrungen hat – frage nach.

Im Falle einer drohenden Ausschaffung, kontaktiere die Kirchliche Anlaufstelle Zwangsmassnahmen auf (siehe «Adressen»).
