# FILZEN

In der Öffentlichkeit darf Dich die Polizei nicht ausziehen (lassen), sie darf Dich aber filzen, also Deine Taschen leeren, Dich abtasten oder in Deinen Mund schauen. Bestehe darauf, dass dies durch eine\*n Polizist\*in Deines Geschlechts geschieht.

Ausziehen darf Dich die Polizei nur auf dem Polizeiposten und nur, wenn dies zu Deinem eignen Schutz resp. zum Schutz anderer Personen erforderlich erscheint oder wenn der begründete Verdacht besteht, dass Du Gegenstände auf Dir trägst, die sicherzustellen sind. Wird von Dir verlangt, dass Du Dich ausziehst, solltest Du nach dem Grund fragen. Du kannst versuchen, das Ausziehen zu verweigern. Solltest Du Dich tatsächlich vor der Polizei ausziehen müssen, solltest Du darauf bestehen, das nur vor Personen Deines Geschlechts zu tun.

Sich-Ausziehen-Müssen kann länger anhaltend ein unangenehmes Gefühl auslösen. Deshalb ist es wichtig, dass Du mit Leuten, denen Du vertraust, über das Erlebte und Deine Gefühle redest (vgl. «Nur gemeinsam sind wir stark»).
