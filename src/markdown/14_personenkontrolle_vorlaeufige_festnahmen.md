# PERSONENKONTROLLE / VORLÄUFIGE FESTNAHME

Die Polizei kann im Zusammenhang mit Straftaten oder allgemein zur «Gefahrenabwehr» Personenkontrollen durchführen. Du musst Deine richtigen Personalien (Name, Vorname, Geburtsdatum, Adresse, Heimatort) angeben, weitere Angaben darfst Du – und solltest Du – verweigern (vgl. «Aussageverweigerung»).

Polizist\*innen tragen meistens Namensschilder (aussser im «Ordnungsdiensteinsatz» z.B. an Demonstrationen). Bist Du selber betroffen, kannst Du nach ihren Namen fragen. Sind sie in zivil (ohne Uniform) unterwegs, so müssen sie sich auf Anfrage ausweisen.

In der Schweiz musst Du keinen Ausweis auf Dir tragen, es empfiehlt sich aber trotzdem, einen dabeizuhaben. Wenn Deine Identität nicht an Ort und Stelle festgestellt werden kann, darf Dich die Polizei im Rahmen einer Personenkontrolle mit auf den Posten nehmen. Dort kann sie Dich etwa fotografieren oder Deine Fingerabdrücke nehmen (vgl. «Erkennungsdienstliche Massnahmen»). Nachdem Deine Identität geklärt wurde, solltest Du sofort freigelassen werden. Die Personenkontrolle darf nur ein paar Stunden dauern. Wurdest Du zwecks Identitätsfeststellung erkennungsdienstlich behandelt, müssen die Daten (sofort) gelöscht werden, sobald Deine Identität geklärt ist.

Anders bei einer vorläufigen Festnahme: Nach einer Personenkontrolle kann Dich die Polizei vorläufig festnehmen, um Dich oder andere Personen zu schützen oder um eine Straftat zu verhindern.

Du kannst aber auch vorläufig festgenommen werden, wenn Du auf frischer Tat ertappt wirst, wenn Dich die Polizei konkret verdächtigt, Du hättest ein Verbrechen oder ein Vergehen begangen oder wenn Du zur Verhaftung ausgeschrieben bist. In diesem Fall musst Du nach höchstens 24 Stunden freigelassen oder der Staatsanwaltschaft vorgeführt werden.

Die Staatsanwaltschaft kann Dich in Haft behalten, wenn Sie beim Zwangsmassnahmengericht Untersuchungshaft beantragen will (vgl. «Untersuchungshaft»).
