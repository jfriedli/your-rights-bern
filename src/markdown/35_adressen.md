# ADRESSEN

### ALLGEMEINE BERATUNG

Kirchliche Gassenarbeit
Bern KGB,
Speichergasse 8,
3011 Bern
031 312 38 68
gassenarbeit-bern.ch
mail@gassenarbeit-bern.ch

### AUSLÄNDER*INNEN

Asylhilfe Bern, Bahn­
höheweg 44, 3018 Bern
031 382 52 72
asylhilfe.ch
info@asylhilfe.ch


Berner Rechtsberatungsstelle für Menschen in Not
Eigerplatz 5, 3007 Bern
031 385 18 20
rechtsberatungsstelle.ch
info@rechtsberatungsstelle.ch


Fachstelle Sozialarbeit
der kath. Kirche FASA
Region Bern. Mittel­
strasse 6a, 3012 Bern
031 300 33 51
kathbern.ch
fasa.bern@kathbern.ch


isa – Fachstelle für Migration, Speichergasse 29,
3011 Bern
031 310 12 70/72
isabern.ch
isa@isabern.ch


Kirchliche Anlaufstelle
Zwangsmassnahmen
Kanton Bern, Postfach,
3001 Bern
031 332 00 50
thomaswenger@weibel-wenger.ch


Solidaritätsnetz SansPapier Bern,
Schwarztorstrasse 76, 3007 Bern
031 991 39 29
info@solidaritaetsnetzbern.ch


Verein Berner Beratungsstelle für Sans-Papiers
Effingerstrasse 35,
3008 Bern
031 382 00 15
sanspapiersbern.ch
beratung@sans-papiers-contact.ch

### KINDER UND JUGENDLICHE

Kinderanwaltschaft
Schweiz
Theaterstrasse 29,
8400 Winterthur
052 260 15 55


### MENSCHENRECHTE

Menschenrechtsverein
augenauf Bern
Quartiergasse 17,
3013 Bern
031 332 02 35
augenauf.ch
bern@augenauf.ch


Demokratische Jurist\*innen Bern (djb)
Postfach, 3001 Bern
djs-jds.ch (inkl. Verzeichnis von Anwält\*innen)
djb@djs-jds.ch

### OPFERHILFE/GEWALT

Beratungsstelle Opferhilfe
Bern, Seftigenstrasse 41,
3007 Bern
Tel 031 370 30 70
opferhilfe-bern.ch
beratungsstelle@opferhilfe-bern.ch


Frauenhaus Bern
Postfach 2126,
3001 Bern
031 332 55 33
frauenhaus-bern.ch
info@frauenhaus-bern.ch


Lantana – Fachstelle
Opferhilfe bei sexueller
Gewalt, Aarbergergasse
36, 3011 Bern
031 313 14 00
lantana-bern.ch
info@lantana-bern.ch


Vista – Fachstelle Opferhilfe bei sexueller und
häuslicher Gewalt
Bälliz 49, 3600 Thun
033 225 05 60
vista-thun.ch
info@vista-thun.ch


Beratungsstelle Opferhilfe
Biel, Silbergasse 4, 2502
Biel, 032 322 56 33
opferhilfe-bern.ch
beratungsstelle@opferhilfe-biel.ch


Fachstelle Gewalt Bern
Seilerstrasse 25,
Postfach, 3001 Bern
031 381 75 06
fachstellegewalt.ch
info@fachstellegewalt.ch


Männer- und Väterhaus
ZwüscheHalt
031 552 08 70
bern@zwueschehalt.ch


LGBTIQ+hab queer bern
Villa Stucki, Seftigenstrasse 11, 3007 Bern
031 311 63 53
habqueerbern.ch/kontakt/


Transgender Network
Switzerland TGNS
Monbijoustrasse 73,
3007 Bern
031 372 33 44
tgns.ch/de/kontakt/


### FAMILIE/TRENNUNG/SCHEIDUNG

frabina – Beratungsstelle
für Frauen & binationale
Paare, Kapellenstrasse
24, 3011 Bern
031 381 27 01
frabina.ch
info@frabina.ch


Ehe- und Familienberatung Bern, Fachstelle für
Beziehungsfragen des
Kantons Bern
Aarbergergasse 36,
3011 Bern
031 312 10 66
eheundfamilienberatung-bern.ch,
info@eheundfamilienberatung-bern.ch


Beratungsstelle Ehe,
Partnerschaft, Familie der
ref. Kirchen Region Bern
Marktgasse 31,
3011 Bern
031 311 19 72
eheberatungbern.ch
bern@berner-eheberatung.ch


### RECHTSBERATUNG DIVERSES

Frauenberatungsstelle
Infra Bern, Flurstr. 26b,
3014 Bern
031 311 17 95
infrabern.ch


Actio Bern – Fachstelle
für Sozialhilferecht, Bern
031 525 34 38
actiobern.ch
info@actiobern.ch


Ombudsstelle der Stadt
Bern, Effingerstrasse 4,
3011 Bern
031 312 09 09
bern.ch/politik-und-verwaltung/stadtverwaltung/
ombudsstelle
ombudsstelle@bern.ch

### PIKETTDIENST STRAF-VERTEIDIGUNG/AUSSCHAFFUNGSHAFT

(Bernischer Anwaltsverband und Demokratische
Jurist\*innen Bern),
Sekretariat Bernischer
Anwaltsverband,
Postfach 1052
3401 Burgdorf
034 423 11 89
bav-aab.ch


### REPRESSION

AntiRep Bern, Postfach,
3001 Bern
antirep-bern.ch
info@antirep-bern.ch


### SEXARBEIT

XENIA – Fachstelle Sexarbeit,
Langmauerweg 1,
3011 Bern
031 311 97 20
079 511 97 20
thailändisch: 079 311 97
11 (Di 13–16 Uhr)
xeniabern.ch
info@xeniabern.ch


### SPORT

Fanarbeit Schweiz
Pavillonweg 3, 3012 Bern
079 617 75 82
fanarbeit.ch
support@fanarbeit.ch


### MILITÄR / WEHRDIENST

Beratungsstelle für Militärverweigerung und Zivildienst,
Gartenhofstrasse
7, Postfach, 8036 Zürich
044 450 37 37
gsoa.ch/beratungsstellen/
civiva.ch, beratungsstelle@zivildienst.ch


### AMTLICHE STELLEN

Bundesamt für Polizei
(fedpol), Guisanplatz 1A,
3003 Bern
058 463 11 23
fedpol.admin.ch


Direktion für Sicherheit,
Umwelt und Energie SUE
Predigergasse 12, 3001
Bern, 031 321 50 05
bern.ch/stadtverwaltung/
sue, sue@bern.ch


Kindes- und Erwachsenenschutzbehörde
(KESB). Kreise siehe: jgk.be.ch/jgk/de/index/direktion/organisation/kesb/kesb*kreise.html


Kindes- und Er­wach­se­
nen­schutzgericht,
Hochschulstrasse 17,
3001 Bern,
031 635 48 06
justice.be.ch


Sicherheitsdirektion des
Kantons Bern,
General­sekretariat,
Kramgasse 20,
3011 Bern
031 633 47 23
pom.be.ch


### ZWANGSMASSNAHMEN-GERICHTE

Liste siehe: justice.be.ch/justice/de/index/justiz/organisation/strafgericht/ueber*uns/zwangsmassnahmengerichte.html


Bundesverwaltungsgericht
Postfach, 9023 St.Gallen
bvger.ch


Eidgenössischer Datenschutz- und Öffentlichkeitsbeauftragter (EDÖB),
Feldeggweg 1, 3003 Bern
058 462 43 95
edoeb.admin.ch

### HERAUSGEBERIN/BEZUGSSTELLE

Kirchliche Gassenarbeit Bern
Speichergasse 8, 3011 Bern
031 312 38 68
mail@gassenarbeitbern.ch


Version vom Februar 2020;
8. über­arbeitete Auflage.


Überarbeitung durch Lena Reusser
(MLaw) und Annina Mullis (MLaw)


Quellen: Deine Rechte, Bern 2012;
Simone Rebmann, lic. jur.;
Franziska Müller, Rechts­anwältin.
