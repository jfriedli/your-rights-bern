# HAUSVERBOT

Private Betreiber\*innen von Restaurants, Bars oder Einkaufsläden dürfen Dir ein Hausverbot erteilen. Sie können dies schriftlich oder mündlich tun. Für eine bestimmte Zeit darfst Du das Lokal dann nicht mehr betreten. Wenn Du es trotzdem tust, riskierst Du eine Anzeige wegen Hausfriedensbruch. Da Lokale öffentlich zugängliche Orte sind, dürfen Dir die Betreiber\*innen nicht für irgendeine Lappalie ein Hausverbot erteilen. Der Grund für das Hausverbot muss von einer gewissen Schwere sein (z.B. Tätlichkeiten, Sachbeschädigungen oder Diebstahl) und die Dauer des Verbotes muss angemessen sein.

Vor Gericht kannst Du verlangen, dass das Hausverbot aufgehoben wird. Wenn es für Ketten von Lebensmittelhandlungen gilt (z.B. alle Migros-Filialen), lange dauert und deswegen Deine Nahrungsmittelversorgung eingeschränkt ist, muss dies das Gericht bei seinem Entscheid berücksichtigen.
