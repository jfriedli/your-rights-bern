# ÖFFENTLICHER RAUM / BETTELVERBOT

Auf Strassen, Trottoirs, Plätzen, usw. darfst Du Dich grundsätzlich frei bewegen und aufhalten. Du darfst im öffentlichen Raum auch zusammen mit Freund\*innen Alkohol trinken (vgl. «Wegweisung/Perimeterverbot»).

Solange Du Dich nicht häuslich einrichtest, darfst Du draussen übernachten – auch im Wald, der zwar meist Privaten gehört, aber dennoch zum öffentlichen Raum zählt. In der Stadt Bern ist das Campieren, also im Wesentlichen das Aufstellen von Zelten, verboten.

Gemeinden oder die kantonale Verwaltung können öffentlich zugängliche Orte mit Videokameras überwachen. Die Kameras müssen deutlich gekennzeichnet sein und die Bilder dürfen maximal 100 Tage aufbewahrt werden, wenn kein konkreter Verdacht auf eine Straftat besteht.

Im Kanton Bern gibt es kein allgemeines Bettelverbot. In verschiedenen Gemeinden, beispielsweise in Spiez oder in Langnau, ist Betteln im öffentlichen Raum verboten.

In der Stadt Bern ist das Betteln im SBB-Teil des Bahnhofs, im öffentlichen Teil der Unterführungen (Neuengass- und Teile der Christoffelunterführung) sowie im Umkreis von 10 Metern um die Bahnhofausgänge verboten.
