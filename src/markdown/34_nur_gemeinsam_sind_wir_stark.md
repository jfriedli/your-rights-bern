# NUR GEMEINSAM SIND WIR STARK

Kontakt mir der Polizei zu haben, kann anstrengend und aufreibend sein. Es kann belastend sein und insbesondere wer Opfer von Polizeigewalt wird, kann für längere Zeit emotionale Folgen spüren.

Das ist kein Zeichen individueller Schwäche, sondern ist ganz «normal». Deshalb ist es wichtig, dass Du mit Menschen sprichst, die Dir nahe sind und denen Du vertraust, wenn Du merkst, dass es Dir nicht gut geht. Wenn nötig kann Dich Dein Umfeld auch dabei unterstützen, Dir eine Rechtsberatung, medizinische Versorgung oder psychologische Begleitung zu organisieren.

Es mag banal klingen, aber Belastungssituationen lassen sich einfacher tragen, wenn das damit verbundene Gewicht so gut es geht auf mehrere Schultern verteilt wird. Lasst uns deshalb solidarisch miteinander umgehen. Hol Dir bei Bedarf auch professionelle Unterstützung.

Nachfolgend findest Du eine Auswahl möglicher Beratungsstellen.
