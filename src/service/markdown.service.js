import Fuse from 'fuse.js/dist/fuse.min.js'

export default class MarkdownService {
  constructor () {
    this.markdownContext = require.context('src/markdown', false, /\.md$/)
    this.chapters = {}
    this.searchIndex = []
    this.markdownContext.keys().forEach((key) => {
      const context = this.markdownContext(key)
      this.chapters[key] = context
      this.searchIndex.push({
        path: key,
        text: context.default
      })
    })
    // fuse configuration
    const fuseOpts = {
      isCaseSensitive: false,
      findAllMatches: false,
      includeMatches: false,
      includeScore: false,
      useExtendedSearch: false,
      minMatchCharLength: 1,
      shouldSort: true,
      threshold: 0.6,
      location: 0,
      distance: 100,
      keys: [
        'path',
        'text'
      ]
    }
    this.fuse = new Fuse(this.searchIndex, fuseOpts)
  }

  getNavList () {
    const nav = []
    Object.keys(this.chapters).forEach((chapterKey) => {
      nav.push({
        title: this.getFirstLine(this.chapters[chapterKey].default),
        link: chapterKey
      })
    })
    return nav
  }

  getIndexPath () {
    return Object.keys(this.chapters)[0]
  }

  getChapterContentByRoutePath (path) {
    return this.chapters['./' + path].default
  }

  getFirstLine (text) {
    return text.split('\n')[0]
  }

  findByText (searchText) {
    return this.fuse.search(searchText)
  }

  forward (currentIndex) {
    return this._getIndex(currentIndex, true)
  }

  back (currentIndex) {
    return this._getIndex(currentIndex, false)
  }

  _getIndex (currentIndex, forward = true) {
    const chapterKeys = Object.keys(this.chapters)
    let index = chapterKeys.findIndex((chapterKey) => {
      return chapterKey.includes(currentIndex)
    })
    if (forward) {
      index++
    } else {
      index--
    }
    index = index % chapterKeys.length
    if (index < 0) {
      index = chapterKeys.length - 1
    }
    return chapterKeys[index]
  }
}
