import MarkdownService from 'src/service/markdown.service'

export default ({ Vue }) => {
  Vue.prototype.$markdownService = new MarkdownService()
}
